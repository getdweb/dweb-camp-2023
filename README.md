# Dweb Camp 2023: Discovering Flows

![people](images/dwebcamp2022.jpg)

Flows are everywhere, but most of us don’t perceive them. Discovering flow means tapping into the most powerful forces around us:

     Nature. Technology. Community. You.

These four flows are all connected, influencing each other. We invite you to join us in the California redwoods to tap into these hidden flows, to amplify our collective impact in the world.

This is the repository for the organizing of DWeb Camp from June 21-25, 2023.

## Get there
![june-21-25](https://img.shields.io/badge/june%2021-25-b1a7ec.svg)

See the [DWeb Camp website](https://dwebcamp.org) for event and registration details. This event has a [Code of Conduct](https://dwebcamp.org/conduct/).

## Fellowships
![march-15](https://img.shields.io/badge/march-15-b1a7ec.svg)

We are looking for community organizers, technologists, educators, researchers, artists, and everyone in between who are not only imagining a better Web, but are building them for and alongside marginalized populations.

Visit the [Fellowships](https://dwebcamp.org/fellowships/) section and apply **before Wednesday March 15, 2023**.

## Ask us anything
![q&a](https://img.shields.io/badge/questions-answers-b1a7ec.svg)

| Session       | Time (in PDT) | Location | Notes |
|:--------------------|:-----------------------------|:---------------------------------------------------------------|:--------------------------------------------------------------|
| Session 1: Ask us anything | `March, 2023 Tue March 7, 2023 8:00 am ` | Online TBD |  |
|

Questions? Write to dwebcamp@archive.org


## Timeline
|  Date    |    Event                                                           |
|:----------|:--------------------------------------------------------------|
| Jun 19-21 | Volunteer build                                      |
| Jun 21-25 | **DWeb Camp**                                                 |
| Jun 25 | Volunteer take down                 |

-----
## Tomorrow

The World Wide Web has always contained revolutionary potential, to identify and address the most pressing challenges of our time. It has enabled us to connect our communities, tackle local issues, and organize across borders to address global crises. 

But its powerful potential continues to be stifled. 

Just this year, one of the largest social networks in the world – relied on by billions of people around the world to share critical information, culture, and news – has become a plaything of the richest human on the planet.

What happens if we continue down this path -- an internet fractured by geopolitics, dominated by surveillance capitalism, and controlled by a powerful few?  Without serious intervention, will there even be a way out? 

So at this year’s DWeb Camp, we’re going to try something different. 

We are taking stock of all the hardware, applications, protocols, and processes that we have already designed and built. We want to see if they’re ready to be truly tested, and to know what works and what doesn’t. We are going to spend the week sharing, scheming, and dreaming together, until we live this new reality, just for a day. 

Join us at DWeb Camp. 

Join us Tomorrow.

-----

## Location: Camp Navarro, CA
![Tux, the Linux mascot](./images/camp-navarro-map.jpg)
